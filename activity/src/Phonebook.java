import java.util.ArrayList;

public class Phonebook {
    // Properties
    private ArrayList<Contact> contacts;

    // Constructor
    public Phonebook(){
        this.contacts = new ArrayList<Contact>();
    }

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    // Getters and Setters
    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    public void setContacts(Contact contacts) {
        this.contacts.add(contacts);
    }
}
