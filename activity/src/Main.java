import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");
        Contact contact2 = new Contact("Jane Doe", "+633213213131", "Makati City");

        Phonebook phonebook = new Phonebook();

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        ArrayList<Contact> contacts = phonebook.getContacts();
        if (contacts.isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {
            for (Contact contact : contacts) {
                System.out.println(contact.getName());
                System.out.println("---------------------------------------------------");
                System.out.println(contact.getName() + " has the following registered numbers");
                System.out.println(contact.getContactNumber());
                System.out.println("---------------------------------------------------");
                System.out.println(contact.getName() + " has the following registered address");
                System.out.println("My home in " + contact.getAddress());
                System.out.println("===================================================");
            }
        }



    }
}